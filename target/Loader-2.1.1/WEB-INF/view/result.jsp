<%--
  Created by IntelliJ IDEA.
  User: Винчестер
  Date: 18.08.2016
  Time: 17:51
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8"  language="java"  session="true"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="inter.language" />
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<fmt:message key="result.text1" var="text1" />
<fmt:message key="result.text2" var="text2" />

   ${text1} ${fileName}. ${text2} <a href="${url}">${url}</a>

</body>
</html>