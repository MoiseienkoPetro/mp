package peter.service;

import com.google.common.base.Preconditions;
import com.google.common.io.Closeables;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.UUID;

public class FileService {
    public static final FileService INSTANCE = new FileService();
    private static final Logger logger = Logger.getLogger(FileService.class);
    private boolean inited;
    private File rootDir;
    private FileService() {

    }

    public java.util.Optional<File> find(String fileId) {
        Preconditions.checkNotNull(fileId);
        File restoredDirectoryWithOneFile = new File(rootDir, fileId);
        if (!restoredDirectoryWithOneFile.exists()) {
            return java.util.Optional.empty();
        }
        File[] list  = restoredDirectoryWithOneFile.listFiles();
            try {
             Preconditions.checkNotNull(list);
               // throw new IllegalStateException("Can not read content of " + restoredDirectoryWithOneFile);
            } catch (IllegalStateException ex) {
            logger.error("Catch exeption", ex);
                throw ex;
            }
        if (list.length == 0) {
            // empty dir and no file?
            return java.util.Optional.empty();
        }
            //  or there are more than one?
            try {
                Preconditions.checkState(list.length>1);
                //throw new IllegalStateException("Have no idea how a few files were uploaded there " + restoredDirectoryWithOneFile);
            } catch (IllegalStateException ex) {
              logger.info("pups", ex);
            }
        return java.util.Optional.of(list[0]);
    }

    public String upload(String fileName, InputStream inputStream) throws IOException {
        FileLock filelock=null;
        RandomAccessFile file;
        try {
            file = new RandomAccessFile(fileName, "rw");
            filelock = file.getChannel().lock();
            Preconditions.checkNotNull(fileName);
            Preconditions.checkNotNull(inputStream);
            final String fileId = generateUniqueId();
            File uniqueFileDirToKeepCopiedFile = new File(rootDir, fileId);
            boolean justCreated = uniqueFileDirToKeepCopiedFile.mkdir();
            Preconditions.checkState(justCreated, "Generated ID is not unique");
            File copied = new File(uniqueFileDirToKeepCopiedFile, fileName);
            Files.copy(inputStream, copied.toPath());
            Closeables.closeQuietly(inputStream);
            return fileId;
        }finally {
            if(filelock!=null&&filelock.isValid()){
            filelock.release();}
        }
    }

    public synchronized void init(File rootDir) {
        Preconditions.checkNotNull(rootDir);
        Preconditions.checkArgument(rootDir.isDirectory());
        Preconditions.checkState(!inited, "Already inited, this method needs to be called once only");
        inited = true;
        this.rootDir = new File(rootDir, "uploaded-files");
        this.rootDir.mkdirs();
    }

    private static String generateUniqueId() {
        return UUID.randomUUID().toString();
    }
}

