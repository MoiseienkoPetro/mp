package peter.web;

import peter.service.FileService;
import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.nio.file.Files;
import java.util.Optional;



@WebServlet(name =  "download", urlPatterns = {"/download/*"})
@MultipartConfig()
public class DownloadServlet extends HttpServlet {
    FileService fileService = FileService.INSTANCE;
    private static final Logger log=Logger.getLogger(DownloadServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileId = normalize(request.getPathInfo());

        try {
            Optional<File> file = fileService.find(fileId);

            if (!file.isPresent()) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

            // TODO: taken from your code, not sure we need it. Guess it is set automatically ?..
            // response.setHeader("Content-Length", String.valueOf(file.get().length()));
            response.setHeader("Content-Type", getServletContext().getMimeType(file.get().getAbsolutePath()));
            response.setHeader("Content-Disposition", "inline; filename=\"" + file.get().getName() + "\"");

            Files.copy(file.get().toPath(), response.getOutputStream());

        }

        catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private static String normalize(String pathInfo) {
        return (pathInfo.startsWith("/")) ? pathInfo.substring(1) : pathInfo;
    }
}
