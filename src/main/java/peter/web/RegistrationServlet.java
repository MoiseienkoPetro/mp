package peter.web;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import peter.user.User;

/**
 * Created by Petro on 28.09.2016.
 */
@WebServlet(name = "Registration", urlPatterns = {"/registration/*"})
public class RegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = new User();
        if (name == "" || surname == "" || login == "" || password == "") {
            request.setAttribute("Error", "All fields are required");
            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
        } else {
            if (user.createUser(name, surname, login, password)) {
                getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
            } else {
                request.setAttribute("Error", "This username is already taken");
                getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
