package peter.web;
import peter.user.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Винчестер on 21.08.2016.
 */
@WebServlet(name =  "started", urlPatterns = {"/started/*"})
public class StartedServlet extends HttpServlet {
    User user;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession httpsession=request.getSession();
        String lang=request.getParameter("language");
        httpsession.setAttribute("language",lang);

        String login=request.getParameter("login");
        String password=request.getParameter("password");
        System.out.println(request.getRequestURI());
        user=new User();
        if(user.checkUser(login,password)){
    getServletContext().getRequestDispatcher("/WEB-INF/view/select.jsp").forward(request, response);}
         else{
            request.setAttribute("ErrorText","The login or password is incorrect");
            getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);
            System.out.println("not found");}
        }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
