package peter.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import peter.service.FileService;

import java.util.Locale;

@WebServlet(name = "upload", urlPatterns = {"/upload"})
@MultipartConfig()
public class UploadServlet extends HttpServlet {
    FileService fileService = FileService.INSTANCE;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file");
        String fileName = filePart.getSubmittedFileName();
        // TODO: do not copy the entire file, just move it, e.g. call write() method since it could be optimized (just move the file on FS)
        String fileId = fileService.upload(fileName, filePart.getInputStream());

        request.setAttribute("fileName", fileName);
        request.setAttribute("url", composeDownloadUrl(request, fileId));
        //Locale text=session.getAttribute("myLocale");
        getServletContext().getRequestDispatcher("/WEB-INF/view/result.jsp").forward(request, response);
    }

    private String composeDownloadUrl(HttpServletRequest request, String fileId) throws MalformedURLException {
        URL url = new URL(request.getRequestURL().toString());
        URL withNoPath = new URL(url.getProtocol(), url.getHost(), url.getPort(), "/download/" + fileId);
        return withNoPath.toExternalForm();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        String root = config.getServletContext().getRealPath("");

        File rootDir = new File(root);

        fileService.init(rootDir);
    }
}