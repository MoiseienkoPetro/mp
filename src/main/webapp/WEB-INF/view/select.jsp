<%--
  Created by IntelliJ IDEA.
  User: Винчестер
  Date: 21.08.2016
  Time: 19:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"  language="java"  session="true"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% String loc=(String)session.getAttribute("language");%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="inter.language" />
<!DOCTYPE html>
<html lang="${loc}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>File Upload Example</title>
</head>
<body>
<div align="CENTER">
    <form accept-charset="UTF-8" action="upload" method="post" enctype="multipart/form-data">

        <input type="file" name="file"   />
        <input type="submit" name="submit" >
        <input type="reset" name="reset" >
    </form>
</div>
